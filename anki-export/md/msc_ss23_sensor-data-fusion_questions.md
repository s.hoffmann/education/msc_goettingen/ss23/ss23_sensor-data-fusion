# msc__ss23__sensor-data-fusion

## msc_ss23_sensor-data-fusion_questions

### List the three main sensors introduced in the lecture.

![](img/paste-9fbfbab14e30970536ccd6273f4abd80533da818.jpg)


### Explain the advantages and disadvantages of the  **Radar**  technology.

![](img/paste-9fbfbab14e30970536ccd6273f4abd80533da818.jpg)


### Explain the advantages and disadvantages of the  **Laser-Scanner**
technology.

![](img/paste-9fbfbab14e30970536ccd6273f4abd80533da818.jpg)


### Explain the advantages and disadvantages of the  **Camera** technology.

![](img/paste-9fbfbab14e30970536ccd6273f4abd80533da818.jpg)


### Describe the two introduced  _sensor fusion_  concepts.

![](img/paste-a2bc34d4c69347c99dc4bd619a9623cfb9449c22.jpg)


### Describe the **fusion levels**  introduced in the lecture.

![](img/paste-f0cd84f05d8f376e9d0087a659d269e027dd87e6.jpg)


### (Weighted) linear least squares  

  * explain the different components
  * give the  **linear measurement equation**
  *  _Important:_  list the assumptions regarding the dimensions of the components, especially for H
  * Give the cost function and the normal equation of x_ls

![](img/paste-b0f38b3a0da26b3a1a1b139bf6320a02741731fe.jpg)  
![](img/paste-13eebba44ef2afbe5476b7dad810df23894c98a7.jpg)


### _Weighted linear least squares_  \- Which assumptions need to apply for the
**measurement matrix** and the **weighting matrix**?

  * H has full column rank
  * W is a semi-positive definite matrix of R_mxm

![](img/paste-7c744705a0416c2fb983a762b92d970697cf2901.jpg)  

![](img/paste-13eebba44ef2afbe5476b7dad810df23894c98a7.jpg)  


### List the characteristics of a symmetric positive definite matrix.

  * symmetric matrix: see slide
  * positive matrix: all Eigenvalues are positive
  * definite matrix: all Eigenvalues are greater than 0

![](img/paste-a42d359ffd2b1d123eded299e300a5079a215c28.jpg)  


### List the characteristics of full rank matrices.

![](img/paste-461c43e961805a3c420a48c24dc02c52f24bb462.jpg)


### Why do we need multiple sensor types?

Sensors have advantages and disadvantages. Using multipe different sensors can
balance these out.


### Name and explain two sensor fusion concepts.

Two concepts are competitive fusion (multiple sensors measure the same space
to increase accuracy) and complementary fusion (multiple sensors measure
different spaces to increase completeness).


### What is a measurement equation? Could you write down the linear measurement
equation in a general form and explain the components in it?

![](img/paste-5cd6b7ba96c3787e429073d8bed502cf5305c252.jpg)


### What are the assumptions for least squares?

![](img/paste-c51f27bab9d838271db46ef3bca28e3aed556178.jpg)


### Summarize the term  **Trilateration**.

![](img/paste-32b2556157a3e2fd6f3aef6d5ad73369d5633a98.jpg)  
![](img/paste-1f17c1c8a6ec1ae80b558775d569e76a50ac4db7.jpg)  
![](img/paste-19c7a01f72ec337e699f2a152a0955356b1f7b51.jpg)  
![](img/paste-38b1e7037338c5107fc45351fc54bdd054ffeff0.jpg)  
![](img/paste-d0b2b78d167f4e39df3b30b3a4ff9bbededbf1b5.jpg)  
![](img/paste-03ce4327a7b2e69e907a6131dfc23a41d7f90144.jpg)  
![](img/paste-f4899c61f96c60890e3eba984f7ccbcda9e5bbb9.jpg)  
![](img/paste-e0b7ed03ec9ef28b44acb39004ac09762775df71.jpg)  
![](img/paste-ac4e1fe8ce213dfe80573de5b0a3b1128d88cf9b.jpg)  


### Summarize the term  **differential GPS**.

![](img/paste-b2098c2693fe3b5a6b8ff1953c670b7e790f2b7c.jpg)


### Describe the problem formulation of trilateration in 2d.

![](img/paste-5a82538c66e18cee8c18a2224a4f7b9e661bce81.jpg)


### Give the formulas for  **nonlinear least squares**.

![](img/paste-9da09ad1bb5d27e67b3815cd6dec8ab31cddde93.jpg)


### Nonlinear least squares - list the possible solution approaches.

![](img/paste-9da09ad1bb5d27e67b3815cd6dec8ab31cddde93.jpg)  
![](img/paste-2c20469b1b0b3b0d15cacf1fac48a71f215da376.jpg)


### Deduce the simple close-form solution for trilateration.

![](img/paste-4d244e58e6907a7ffddae06562313cb2572973e9.jpg)  
![](img/paste-0ff27a238ce657984e1088f1b1161c3024667825.jpg)


### Describe the term  **Multilateration**.

![](img/paste-f34e40461a19706f2d390b9904df818f02d39df8.jpg)  
![](img/paste-d04ec3de7e6da7c343c83cd1bba7d096ef8a95e6.jpg)  
![](img/paste-2d5d511255b3620a51c0d3f403f85365959c4e6e.jpg)


### State the  _problem formulation_  of  **Multilateration**.  

  * Give the measurement equation
  * What's the main advantage towards trilateration?

![](img/paste-2d5d511255b3620a51c0d3f403f85365959c4e6e.jpg)


### Describe the  **Multilateration**  problem formulation in 2d.

![](img/paste-b901e505a41be13b7c51c132c73f350222d260c9.jpg)


### Describe the term  **Triangulation**. Give the _stacked measurement equation._  

  * Which parts are _desired, given?_

![](img/paste-4de4678cce7486dd4b7cf00d4da2c0d907320f8b.jpg)  
![](img/paste-f684885b1f3ccefb396f979ad899019a23c6a6d9.jpg)


### Taylor Series Expansion - give the main equation in 1d.

![](img/paste-fbba7daf5089a5ce9d5b6ba48dea91d5e4f2209e.jpg)


### Give the basic equations of the  _Newton Method_ ** ** for **  Root Finding.**

![](img/paste-f806d6dacc3125a83a31f221fef3e864df082ae3.jpg)  
![](img/paste-fbba7daf5089a5ce9d5b6ba48dea91d5e4f2209e.jpg)


### Give the basic equations of the  _Newton Method_ ** ** for **  Minimization**.
What's the main problem here?

![](img/paste-6ece77d27b57b07ee6cc0f78d04d3b6f931857db.jpg)


### Which conditions need to apply in order for the  _newton method_  to work?

![](img/paste-39fc4d07a571ec68f313de892e2d594c6ef5878d.jpg)


### What is a  _jacobi matrix?_

![](img/paste-898a557fc464cfc8711885ba8cdfd7676c0b8e2b.jpg)


### State the main **objective**  and the different  **steps of the Gauss-Newton
algorithm**  in a multidemensional case.

![](img/paste-53616976e4d004822c85c4799698ac4cff92ac43.jpg)  
![](img/paste-564b636a38c112457ebc69727052bb2092412d15.jpg)


### In one sentence summarize the  **Fisher approach**. Explain the corresponding
measurement equation.

![](img/paste-cc6eae801db2d6dcca5021be7e784eb2a0bda68e.jpg)


### Visualize a two dimensional stacked measurement equation with a plot.

![](img/paste-724e441de5e03d9e3e6c4e9bd53807aaa1c2131a.jpg)


### Define the terms  _estimator  _and  _ **linear**  estimator._

![](img/paste-801abcb932f5837995b389b686df357d8406cffb.jpg)


### Define the term MSE. What special condition does apply here for unbiased
estimators?

![](img/paste-801abcb932f5837995b389b686df357d8406cffb.jpg)


### Describe and give the formulas for the  **bias-variance decomposition**  in
the fisher setting.

![](img/paste-7fc1b0ee0de22cd6d953505690fe3acde65f2936.jpg)


### Define the terms  **MVU**  and  **BLUE**.

![](img/paste-7fc1b0ee0de22cd6d953505690fe3acde65f2936.jpg)  
![](img/paste-c3ff03bce2ada48cb7c9e9d2fd24a419018ed1ea.jpg)


### What's the MSE in case of an unbiased estimator?

![](img/paste-c3ff03bce2ada48cb7c9e9d2fd24a419018ed1ea.jpg)


### State the  **unbiased condition**  of a  _linear estimator_. How can you check
if an estimator is unbiased?

![](img/paste-27f8db9b32f25a613f68cd7b7f15ff1ab538d673.jpg)


### How can one calculate the covariance of a linear estimator?

![](img/paste-27f8db9b32f25a613f68cd7b7f15ff1ab538d673.jpg)


### Is the linear LS a  _linear estimator?_

![](img/paste-a77ee282e0763301a5a35d0b18656dd16845bfd8.jpg)


### Is the  _linear LS  _ **unbiased**?

![](img/paste-a77ee282e0763301a5a35d0b18656dd16845bfd8.jpg)


### Is linear LS also the best linear unbiased estimator, how can you prove it?
What needs to apply?

![](img/paste-a77ee282e0763301a5a35d0b18656dd16845bfd8.jpg)  
![](img/paste-b0f539dac966cd0b90c7e4f37f4533e32cacffe0.jpg)


### Can you explain how Time-difference-of-Arrival works? Which conditions needs
to apply?

![](img/paste-70daa1f98cf6f578f52fa20627732c72f527ecc8.jpg)


### What is an estimator? State the form of a linear estimator and state the MSE.

![](img/paste-be7aff7fce7b4731d21270b2afe349d0f138a756.jpg)


### How can the MSE be decomposed?

![](img/paste-b4d604dd40efcadc612892882667e224658c4a24.jpg)


### ![](img/paste-67aa821ac9ffd37d9b01d6354e7dc6cb7e3168f0.jpg)

![](img/paste-f8bfa6d50ef9ec4d1ba58b62c2721825375c71ca.jpg)  
![](img/paste-f23f6cdb1b90ba4bb981793de028ef8d9f34ee0d.jpg)


### Describe the  _centralized fusion architecture_  with a picture. What are the
main drawbacks of this approach?

![](img/paste-3596e5e4e07078c484dc19cf85528ba8581c5cce.jpg)


### Sketch the  _distributed fusion_   _architecture_. List the three main
properties and the main advantage of this approach.

![](img/paste-602cf5300bdb3b899226c3fea9fee57c3fb555dc.jpg)


### Sketch the  _decentralized fusion architecture_. List the 4 main properties, 3
advantages and 1 challenge.

![](img/paste-5dd63eeb849c6ea050c8898ff68b9022d0cd1dd3.jpg)


### Describe the terms  _covariance bounds_  and  _correlation coefficient_  with
a sketch.

![](img/paste-ab3ad7b31540ee3da18442a9ee603bb76cd0051a.jpg)  
![](img/paste-d49a4ddb75f18c1b69ffc30796918b7e67dc5acd.jpg)


### How can you determine the optimal  _larger_  coefficient matrix for a given
matrix with unkown correlation coefficient?

![](img/paste-9f696d3eb466c15383be475ec6da1964e7d78651.jpg)  
![](img/paste-9f696d3eb466c15383be475ec6da1964e7d78651.jpg)


### Explain the  _prior probability density function_  and the _likelihood
function  _of the  **Bayesian Approach**.

![](img/paste-d79fbf1814e6110b8e88fe7c903bd7d29ab3f1f7.jpg)


### Whats the difference between the  _prior  _and the  _posterior_  in the
_bayesian approach?_

  * Prior: Represents initial beliefs before observing data
  * Posterior: After observing data, we update beliefs about parameter, given both the prior distribution and the observed data unsing bayes' theorem ( _P(A|B) = (P(B|A) * P(A)) / P(B)_

![](img/paste-d79fbf1814e6110b8e88fe7c903bd7d29ab3f1f7.jpg)  


### Explain the  _joint probability density function_  in the  _bayesian
approach._

![](img/paste-d79fbf1814e6110b8e88fe7c903bd7d29ab3f1f7.jpg)


### State the notation of the **bayesian mean squared error**.

![](img/paste-d6f9ce3f5e85b125f35cc6cc3e96ea393fde5979.jpg)  
![](img/paste-51ac682861ba20f334a5c85d1ef5a6265964a171.jpg)


### Explain the **bias-variance-decomposition**  in a  _bayesian setting._

![](img/paste-51ac682861ba20f334a5c85d1ef5a6265964a171.jpg)


### Summarize a  _ultrasonic distance sensor._

![](img/paste-20b67ad91c48266217e9e55a3ee3e52a8c182e96.jpg)


### Describe the measurement error in a Kalman setting.

![](img/paste-8ca9f1d2d58549f5bebb9999c0ac40d76de54d25.jpg)


### Describe how to fuse two noisy measurements in a 1d Kalman setting.

![](img/paste-72ca1698924903ee1f090eb40c7e3d1571359b43.jpg)


### Describe the  _time update_  step in a 1D  _Kalman setting._

![](img/paste-53b01c64d91a167e8c074e6da8f888c384256ec7.jpg)


### Define the  _discrete-time motion model_  of a _Kalman filter._

![](img/paste-53b01c64d91a167e8c074e6da8f888c384256ec7.jpg)


### Describe the different steps of a Kalman filter.

![](img/paste-adc9e3dcb62fa30814b42865c5312bbbabf33831.jpg)


### For which systems is the Kalman filter  _optimal_  and for which is it
_BLUE?_

![](img/paste-adc9e3dcb62fa30814b42865c5312bbbabf33831.jpg)


### Give an overview over all needed formulas in a 1d Kalman filter.

![](img/paste-83c4b8505f7890881ac1567bdae07d7eb73947f9.jpg)


### Why is the main process from the lecture called **Kalman filter**?

Because it filters out the noise


### Visualize a  _discrete time system_.

![](img/paste-9bcfeb3d6c4a200a22935b3f2077b6af113ef610.jpg)


### Describe the State space model from the  _kalman filter_.  

  * How may it also be called?
  * Explain the five main components.

![](img/paste-568011e8e98c04f9146e3b4ef0089cea936a3f78.jpg)


### In which setting is the kalman filter the most effective?

The kalman filter is taylored to linear systems, corrupted by additive noise.


### Describe the **process model** of the  _Kalman filter._

![](img/paste-2ace8d9b025cda143ccc4bbcb1dd267a4255c597.jpg)


### Define the restrictions for the noise in the  _process model_  from the
_Kalman filter._

![](img/paste-fc003567db83cbcb3e10d926d57ae65c96d2f114.jpg)


### Describe the **measurement model**  from the  _Kalman filter._

![](img/paste-6f8ea34679797805ee117cdf5c106aa06548895f.jpg)


### Describe the restrictions on the error in the  _measurement model_  from the
_Kalman filter._

![](img/paste-6f8ea34679797805ee117cdf5c106aa06548895f.jpg)


### Give another overview over all available Kalman filter formulas in the
n-dimensional case.

![](img/paste-b2509bb74158604b75ba4fea38c6f36a76c16d49.jpg)  
  
Recap:  
![](img/paste-2bedb2cf129efdbf66ef9ea627b2f4d22fe1cf1c.jpg)  
![](img/paste-5d791c01a28629ca2a89e53e93b70bc7f8ffb149.jpg)


### Explain the formular for the **Kalman gain**.

![](img/paste-b2509bb74158604b75ba4fea38c6f36a76c16d49.jpg)  
  
Recap:  
![](img/paste-eaf367f1a6988d6ac99439d59e7e191e14dbf885.jpg)


### Draw a sketch regarding the covariances in the _Kalman filter_. Give an
intuitive explanation about the steps involved.

![](img/paste-f86be120f8423471cc2f33ee62cfefd9dac24ff8.jpg)


### What can be said about the involved distributions in the Kalman filter?

![](img/paste-db64e78403e9c61e2cb79e90df233dfbc6ff9108.jpg)


### Give the formulas for the time update in the  **extended** _  Kalman filter._

![](img/paste-48bc8e17b89afd899a0a9618a1839849d420220f.jpg)  
  
![](img/paste-248f0999ab13e4549807c33dfdb01f5616bc0ac3.jpg)


### Give the formulas for the measurement update in the  **extended** _  Kalman
filter._

![](img/paste-a82ece0a22b961a71346e8798efabd7e792a9c77.jpg)  
  
Recap:  
![](img/paste-76b70d5d71721c3fb4c623b2f6b20b1b58c666f4.jpg)  
![](img/paste-ef516937a0de47a316193e6671d3e4c5e5ac35e1.jpg)  
![](img/paste-9521b8f2e338ddf647045c571e736105bec5f58c.jpg)  
![](img/paste-8edec563e541e504c81f8172f4f2a83e91774e3e.jpg)


### Give an overview over the formulas of the extended Kalman filter.

![](img/paste-0678305e14a755c3b82f6281641de739c89a8f07.jpg)  
![](img/paste-a82ece0a22b961a71346e8798efabd7e792a9c77.jpg)  
  
Recap: General Kalman Filter Formulas  
![](img/paste-b87c92a9ce7d9131bb0d4effe30dceca83d03caa.jpg)


### In a white noise acceleration model describe the form of the  

  * state vector
  * system matrix
  * noise gain
  * covariance of the noise gain

![](img/paste-106cdb8a50601f04ab742a2a9e2471f61834cf5a.jpg)


### Explain the two dimensional model for the white noise acceleration model.

![](img/paste-8929b7a415cc2071efe1e5fd5aa26c9475cd8a04.jpg)  
  
Recap:  
![](img/paste-bb68baa400d53d71fba4d6da5ee96e6dff52ee91.jpg)


### Define the term **  imprecise information**. Give a example.

![](img/paste-a4cd9f6e8ca6f4b8fc139fea425066f1cb2241df.jpg)  

  * aleatory: zufällig / riskant


### Define the term **aleatory uncertainty**. Give an example.

![](img/paste-a4cd9f6e8ca6f4b8fc139fea425066f1cb2241df.jpg)


### Define the term **epistimic uncertainty**. Give an example.

  * we simple do not have enough information about a situation / phenomen to output qualitative information

![](img/paste-a4cd9f6e8ca6f4b8fc139fea425066f1cb2241df.jpg)  


### Define the term **vague information**. Give an example.

![](img/paste-a4cd9f6e8ca6f4b8fc139fea425066f1cb2241df.jpg)


### Given an overview over the classification of  **uncertain   _imperfect_
information**.

![](img/paste-42b2b6947cc76cd963b234ccead21ca722ed3eec.jpg)


### Define the term **  crisp set**. Give an example.

![](img/paste-a64f72c8a52e5fba1dcf67dd777ea40d00257ca4.jpg)


### Describe the  _representation_  of sets. Give the notation of  

  * the enumeration of elements
  * the definition by property
  * the characteristic function

![](img/paste-be660aa2b97c5240ed75afe76966809792f45893.jpg)


### Describe the  **fusion with imprecise   _set-based_  uncertainty**.

![](img/paste-ac9e34eee575fe6e17e0b670ab12d6a9396a193e.jpg)


### Define the term  **fuzzy set**.

![](img/paste-18a74742297f0701d7285d07ab365b4875a9cfb1.jpg)


### Define the following operations on  _fuzzy set_  visually as well as with the
corresponding notation.  

  * complement
  * union
  * intersection

![](img/paste-4b5a406a0b5b10c7da98d65fd87083f6c20b9b17.jpg)  
![](img/paste-d62acf2e62035672d4e4033b1307c152f8beefd2.jpg)  
  
Recap:  
![](img/paste-18a74742297f0701d7285d07ab365b4875a9cfb1.jpg)


### Describe the  **bayesian classification**.

![](img/paste-569a984a0021ab18758249033a20935ac8e57b3f.jpg)


### Describe the  **recursive bayesian classification**.

![](img/paste-01529ede3ddbf7c49a804a1a2510cb8f1e822c49.jpg)


