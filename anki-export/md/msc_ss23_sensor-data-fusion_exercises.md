# msc__ss23__sensor-data-fusion

## msc_ss23_sensor-data-fusion_exercises

### ![](img/paste-4e3319ded451cebc971d0d546a4f7215a2fd6f94.jpg)

![](img/paste-ef0d56ad1209e24bf44925d24333187ff2b55616.jpg)  
![](img/paste-b07ca56b128eb13755d8049e4eac11fd97b5bb05.jpg)  
![](img/paste-9db2f9127ff6b1ff36355c9832213d9aca4d3027.jpg)  
![](img/paste-36ec9e033077091657a1bbbb25dc8a6e07c89ff5.jpg)


### ![](img/paste-4d9fa16b679af818cf63336cf7bedb2f922d90a7.jpg)  
![](img/paste-2d014eb8a1f60edb703f9388058ab7f7f09e321a.jpg)

![](img/paste-f284d0f3588f323fa4ac023d03d55b2d0b972525.jpg)


### ![](img/paste-c335a142ff811d7a10711be44908ba5e3ddd2215.jpg)  
![](img/paste-3566b41a69f5a074ed9d18b4be073a29aa8b471d.jpg)  
![](img/paste-fb05fbff9064587bb613fd33162bba61fe19b527.jpg)  
![](img/paste-55c81a9b1340a7c608bd2399a66ce95b950939d7.jpg)

1a)  
![](img/paste-f9e49ec2ae994440c12a60b6d7d618edb4aef599.jpg)  
![](img/paste-182df417b8cae87bd754ff8e8a7834ddf904fdd0.jpg)  
  
1b)  
![](img/paste-494463ec1e8efe4648943d9195f85fb46d4af989.jpg)  
  
1c)  
![](img/paste-d074c037813ff0704520a7c1e254b6aef805d0c1.jpg)  
  
1d)  
![](img/paste-7470f00745e4de871ca14ac67afbf09b249ac036.jpg)  
  
1e)  
![](img/paste-f482b36f51b7db2daf5caad67bd904ac223dbd38.jpg)  
![](img/paste-d4c3899f8784cf2f6466c6427d4b01dc2f4e0b14.jpg)


### ![](img/paste-c1b5770aa7e6dfb495e05d9d9f5479af97a13a8b.jpg)  

  * State the  _measurement equation_  
  * find the position of the robot

![](img/paste-d8ae25f0ef3881914b749a91cb200f296163e3be.jpg)


### Recap: What are the assumptions for least squares?

![](img/paste-f6624469545bff3c5ffbeef11995d92902e7e79c.jpg)


### What is the objective/cost function of least squares?

![](img/paste-b21dacdec47137f53d5b11eeae1b9736e72c06af.jpg)


### How does trilateration work?

![](img/paste-264e900639a6ec222fab2774fc5cc0ceeedafe38.jpg)


### ![](img/paste-8dae04f227d1a7a82fb9f49ec6bbb813e6ac25e8.jpg)

![](img/paste-90dd114b5d72064e7c4400141d91805365e71852.jpg)  
![](img/paste-f03fc066c47da4ad01abf03da296dfcd3d3b1f70.jpg)  
![](img/paste-b93559047f29350f747838e8f3b3312a4a7686f8.jpg)


### ![](img/paste-2d0ec2e459c755ac0060336a3f5a4b06923cec55.jpg)

![](img/paste-66277c54236cd26a2764ec425294ec270576c08f.jpg)  
![](img/paste-a305809846abbc71e440dbae0d4076c5d05a344b.jpg)


### Can you explain how Triangulation works? Why can’t you use the normal equation
to solve it?

![](img/paste-26bddb7a4871c841a0d03ca0b87bd605ea7642f9.jpg)


### Can you explain the steps of the Gauss-Newton algorithm? (multidimensional
case)

![](img/paste-4f38a4b5c6b55e76b8ffb84598ab083e26052f91.jpg)


### What is a typical use for closed-form approximations?

![](img/paste-f92efb555bf67c1a144576fae251b8a605acffaf.jpg)


### Recap: Nonlinear least squares.  

  * List the assumptions
  * State its objective
  * What are the general solution approaches?

![](img/paste-c9813c38d6abaf3a642e381f5b19cf38697c0b10.jpg)


### Explain the  **bancroft**  solution for  _trilateration_.  

  * Give the squared and linear measurement equation
  * Why is the bancroft solution so useful?

The bancroft is useful when the system is not overdetermined.  
  
![](img/paste-be2fc1c8b1b9bc495dd5cd477c7dd73752d443b1.jpg)  
![](img/paste-c001f8e7b1113efbe850f8b33f0e0a71c1f46d4c.jpg)  
![](img/paste-0e57fbbe1b79eeee847abc12fe4e81aee81d6d36.jpg)


### Describe the atan2 function. When should you use it?

![](img/paste-724a68fa525c4005ebad8b87f7ed8ac6eea82bbb.jpg)


### Triangulation  

  * State the desired and given conditions.
  * State the stacked measurement equation using the atan2 function.

![](img/paste-57eb766816e0fbd9c5da8d7453eb457c9d8029bf.jpg)  
  
  
Recap: atan2  
![](img/paste-708f13ddaf34f350d5e03a05ca19c9e485a84303.jpg)


### Triangulation - give the closed-form solution using linear LQ.

![](img/paste-57eb766816e0fbd9c5da8d7453eb457c9d8029bf.jpg)  
![](img/paste-6d47bb1b54be57b0fd3291489c40af52b0497980.jpg)  
  
Recap: atan2  
![](img/paste-708f13ddaf34f350d5e03a05ca19c9e485a84303.jpg)


### Assume you receive the following angular measurements in radian from two
transmitters:  
  
![](img/paste-d228ffe5ce4b94bed98f30dfd77ef916cb25919b.jpg)

![](img/paste-6ca5de3f1bb1107d26b5b5f8b10828de636b20b4.jpg)  
![](img/paste-bee2a32dea394754849c5c38a7845c5f1a5f3643.jpg)  
![](img/paste-349e55216ddc190a42ea7ead6f996dc4946dd4f3.jpg)


### ![](img/paste-0a971b4b7215212a810f13bff63a8ab5e99372d8.jpg)  
![](img/paste-d6f49a9d1742e8e35c59ebd7df7fc700a52e4a29.jpg)

a)

In Homework 02, we had 4 measurements, of which one was subtracted from the
others, letting us still determine a 3D position. This time, we only have 3
measurements, so we could only calculate a 2D position estimate using the
reformulation from last time.

  

b)

![](img/paste-4a945d6b55778277876f0f709412c39a48f12551.jpg)  

  

c)

![](img/paste-8951a9d5859bbc629276122c6afbfd57bc17cdef.jpg)  


### ![](img/paste-7d93accda7ab1783f19f424579868030186354f0.jpg)  
![](img/paste-ebadbb286d84565b5609956578c795c7c1051875.jpg)  
![](img/paste-9808c4d3ca3e2b36a5dfea8d319e3b2a61c5a853.jpg)

![](img/paste-7957fc0a8c705b67093ac33ae33e5c13c72dd826.jpg)  
![](img/paste-984134c7a682afc377338eb9b17c93d48c465b94.jpg)  
![](img/paste-a0782efdb092ba9c796bab5bccf861e5f6a305bb.jpg)  
![](img/paste-eb730139aecb12ea180d37ac7b1637a45bc35f7c.jpg)


### ![](img/paste-2c15b2dce30fe376fe1027c2f2bd7d9f0e8ec9d4.jpg)

a)  
![](img/paste-f9a9c35f79f587570cb7c2a1c76d596cf0fa5882.jpg)  
  
b)  
![](img/paste-44afe5c6582fc58cca0633f50fbc809f3f2534f2.jpg)  
![](img/paste-ae872f88dfbe840c7d21fd489d084913c0526aa6.jpg)  
  
c)  
![](img/paste-ffba30264140bbcc64a02ff8c6333d0f924fc297.jpg)  
  
d)  
![](img/paste-dbfb2ccaddb64c8e11389ca2c49e134292756867.jpg)  
  
e)  
![](img/paste-22817f2e10fc4aef7a5aeef879534121515d61cf.jpg)


### What is BLUE?

![](img/paste-76c7e6ab3a5eba77f0d5f32061e549829f6c3dc2.jpg)


### What are types of sensor network topologies?

![](img/paste-bdbaf28c6cf302e3c74e19ae6e6ebb5d384875e1.jpg)  
![](img/paste-d734210008cb7a4cbbe1518c8b5949ce12c757b1.jpg)  
![](img/paste-8d7a41bcf4cf37583039b7fec594c0d8870e4172.jpg)


### How can unknown correlation between estimates be handled?

![](img/paste-0c9207d8d03496dc13377abae068b449608e6393.jpg)


### Is the LS a BLUE?

![](img/paste-51db647b5b9781dde52126d9a16c31c6e5958ab2.jpg)  
![](img/paste-be0cdbb473b162b512e1f0adae6c7dfe1dc66c85.jpg)  
![](img/paste-1610ca818f91ad5b2005cb76015ec87989cfe8b6.jpg)


### ![](img/paste-2bf2d5b740ab71ccee77f8d9b2ed2b6e4f683652.jpg)  
![](img/paste-a5290cdbdccdd05bad4b6532daf20dac14415ca3.jpg)

![](img/paste-c73b69e7010f3bc69addce2bfaf4370169a32f06.jpg)  
![](img/paste-f28ada20868b07444898460304bf3ccc67d2b4d5.jpg)


### ![](img/paste-f269b24513b42193cf3166e18ae803ecfb52b9bf.jpg)

![](img/paste-0cfec243a92ff987495770e5f99181de6cce03bc.jpg)  
  
![](img/paste-508677c8bf33ae53242f8471d761e70225a90fbd.jpg)


### ![](img/paste-051ad632a5d897f8a77a726fb6ff3af50cb40e10.jpg)

a)  
![](img/paste-107473773918632c499d86db423a4c0cbae90886.jpg)  
  
b)  
![](img/paste-bff37225684834a171d97994183d67282e3394b5.jpg)  
![](img/paste-0724b8eb8b632c98f25b4feab0fb9b85db0f0836.jpg)  
![](img/paste-25579e95298ee9bd1dfc4a1b19718b3fcd37afae.jpg)  
![](img/paste-26f69b1cc298fc423907c3b96a296ddb20aca854.jpg)


### What is the difference between Bayesian and Fisher approach?

![](img/paste-c93232263c29f87e50e2376de9baaa13523c5fec.jpg)


### How can you measure the quality of an estimator in a Bayesian setting?

![](img/paste-e9431816c461c5d0f196ed0425b0b144bd8887f0.jpg)


### What is the optimal Bayesian estimator and how can you compute it?

![](img/paste-840444d95af74c90752a572d2ab5f63ca27fc41a.jpg)


### ![](img/paste-16263cd8dba79bfbe81704882154d447a10511a4.jpg)

![](img/paste-0a23587cac43e56c9e73966105049a0f92b63c85.jpg)


### ![](img/paste-e93473fca2cd798116d15ce09d4311b3e08b8b8c.jpg)

a)  
![](img/paste-c4a267aef556a4c6d3312ef1d297e0403cf0293a.jpg)  
  
b)  
![](img/paste-344b2f61ac1342cb47b8eab9efd02a1a49b9e971.jpg)  
![](img/paste-2fb5a6245e8b022fbde461cc4f42f14bf148df02.jpg)  
  
c)  
![](img/paste-79675053de4456c5bda98bdfc3dc03a0fe7d855d.jpg)  
![](img/paste-0be54f5949444725c6ca28bc67e6267eedc79c2b.jpg)


### ![](img/paste-72ac38d2144e28625b284a64268d86ec566d68ff.jpg)  
Calculate the answer.

![](img/paste-19e8743559cef473fc9ecd7e268c7767cf5a6ccd.jpg)


### Why is it difficult to obtain the optimal Bayesian estimator?

![](img/paste-7ae98ddd041ce278e3291de04aacc8b2e19827cf.jpg)


### What happens in the Kalman filter update for H=I if the noise of the prior is
much higher than the measurement noise and vice versa?

![](img/paste-b7f6202de858e836c99046728d33fbfd5f5aa813.jpg)


### What can be said about the posterior distribution if all other distributions
involved are Gaussian?

![](img/paste-b56164cbf5df093c38138d1c6a813a730a3ff9d2.jpg)


### ![](img/paste-d250ec69eb3ee7748e709f192c5765670d5171c2.jpg)  
![](img/paste-a0c6dc52998c1d5f2219a0379001d437562d7d38.jpg)

a)  
![](img/paste-2e96033621817afd9f7228a92516f270a915edf5.jpg)  
  
b)  
![](img/paste-a3363af1d576150d1b4cffa20c94ac2ed6b7e745.jpg)  
  
c)  
![](img/paste-d72af837aabb366d71ad2f3813e9f3cc1fbdbb25.jpg)  
  
d)  
![](img/paste-92e03c81a04d8ea9226a2ec8aa7fe60fd49d4690.jpg)  
![](img/paste-4efefd75f178bf67053e45ef72145f53ba2ab802.jpg)  
  
e)  
![](img/paste-732e40b00694c68d6c92bd16ebf8eae6910c2698.jpg)  
![](img/paste-3a1235402414ffcd2b791cbb044eca6b96a6be43.jpg)  
![](img/paste-9a453372d3a93329a61f999a96bb507e669501b9.jpg)


### What are the two special cases which need to be fulfilled for the Kalman
filter to be optimal?

  * linearity
  * Gaussian densities


### Explain the two  _special cases_  in the  _Kalman filter._

![](img/paste-403dda8e99faa1d78dcff177d1a6fdd43c3ecf92.jpg)


### What happens to the state variance during measurement update? What happens
during  
time update?

![](img/paste-92693e82067f3775ab07da3a50441814e58aefe1.jpg)


### Assume during the time update, the state moves with a noise corrupted
velocity. What would happen to the state covariance if we want to estimate the
state for T -> infinity?

![](img/paste-ea92cf04ed207f1189019733bcc3d859d2878e0c.jpg)  
  
Recap:  
![](img/paste-e2aaed30d56269ad762157348b693008df549d6f.jpg)


### What is the idea of the EKF? Describe it using the time update formulas.

![](img/paste-081125f9fd6e2ccc50a2d556c2892e33788d2ba5.jpg)


### ![](img/paste-c492cf031ff282d95adb1b3c3160246708dfb4ac.jpg)

![](img/paste-5dfb950716ee3c1a4e9b952b22450b8474a3680c.jpg)  
![](img/paste-fa79915d83989f8cfb67b5881313ee0255e61dc8.jpg)


### ![](img/paste-543c5a131a4b78169d814ec3de887e285a61f945.jpg)

TODO Lsg. einfuegen


### Define the conditions for  **zero-mean white noise process**.

![](img/paste-621166e176b7d22523229a62e65f86df9101bf96.jpg)


### Describe the  **discrete white noise velocity model**.

![](img/paste-01cb0133375c8cfc4a7d4e61efb2c9ce2b845330.jpg)


### Describe the  **white noise acceleration model**.

![](img/paste-8230ea3d42253bd0c8b6ddd24d1467bda9e8c75b.jpg)  
![](img/paste-106cdb8a50601f04ab742a2a9e2471f61834cf5a.jpg)


### Can you intuitively explain the Kalman filter?

![](img/paste-c117f111bf9bcf0f0b4e157147b7eb7ed73f10c2.jpg)


### What are disadvantages of the EKF?

  * Might diverge for severse nonlinearities
  * need to compute jacobians


### Can you explain the white noise velocity model?

![](img/paste-59892725c6b8e916a28f9fcb7402d139569bb3dd.jpg)


### What is the assumption of the noise during sampling periods in the white noise
acceleration model?

The noise is constant during sampling periods.


### ![](img/paste-75dd6c5a58f6b6a73b9f87b73e85b1320089cb87.jpg)

![](img/paste-081562e12a1909b554c8b296f0d65d5dbc20d9ea.jpg)


### ![](img/paste-952ceab02fc66e9e91a0dfb5d8ba4d4adbd5671a.jpg)

TODO Lsg. einfuegen


