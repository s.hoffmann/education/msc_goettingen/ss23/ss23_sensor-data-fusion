# msc__ss23__sensor-data-fusion

## marked

### Describe the two introduced  _sensor fusion_  concepts.

![](img/paste-a2bc34d4c69347c99dc4bd619a9623cfb9449c22.jpg)


### (Weighted) linear least squares  

  * explain the different components
  * give the  **linear measurement equation**
  *  _Important:_  list the assumptions regarding the dimensions of the components, especially for H
  * Give the cost function and the normal equation of x_ls

![](img/paste-b0f38b3a0da26b3a1a1b139bf6320a02741731fe.jpg)  
![](img/paste-13eebba44ef2afbe5476b7dad810df23894c98a7.jpg)


### Name and explain two sensor fusion concepts.

Two concepts are competitive fusion (multiple sensors measure the same space
to increase accuracy) and complementary fusion (multiple sensors measure
different spaces to increase completeness).


### Nonlinear least squares - list the possible solution approaches.

![](img/paste-9da09ad1bb5d27e67b3815cd6dec8ab31cddde93.jpg)  
![](img/paste-2c20469b1b0b3b0d15cacf1fac48a71f215da376.jpg)


### Deduce the simple close-form solution for trilateration.

![](img/paste-4d244e58e6907a7ffddae06562313cb2572973e9.jpg)  
![](img/paste-0ff27a238ce657984e1088f1b1161c3024667825.jpg)


### What is the objective/cost function of least squares?

![](img/paste-b21dacdec47137f53d5b11eeae1b9736e72c06af.jpg)


### State the main **objective**  and the different  **steps of the Gauss-Newton
algorithm**  in a multidemensional case.

![](img/paste-53616976e4d004822c85c4799698ac4cff92ac43.jpg)  
![](img/paste-564b636a38c112457ebc69727052bb2092412d15.jpg)


### Describe and give the formulas for the  **bias-variance decomposition**  in
the fisher setting.

![](img/paste-7fc1b0ee0de22cd6d953505690fe3acde65f2936.jpg)


### Explain the **bias-variance-decomposition**  in a  _bayesian setting._

![](img/paste-51ac682861ba20f334a5c85d1ef5a6265964a171.jpg)


### How can you measure the quality of an estimator in a Bayesian setting?

![](img/paste-e9431816c461c5d0f196ed0425b0b144bd8887f0.jpg)


### For which systems is the Kalman filter  _optimal_  and for which is it
_BLUE?_

![](img/paste-adc9e3dcb62fa30814b42865c5312bbbabf33831.jpg)


### Give an overview over all needed formulas in a 1d Kalman filter.

![](img/paste-83c4b8505f7890881ac1567bdae07d7eb73947f9.jpg)


### Why is it difficult to obtain the optimal Bayesian estimator?

![](img/paste-7ae98ddd041ce278e3291de04aacc8b2e19827cf.jpg)


### What happens in the Kalman filter update for H=I if the noise of the prior is
much higher than the measurement noise and vice versa?

![](img/paste-b7f6202de858e836c99046728d33fbfd5f5aa813.jpg)


### Give another overview over all available Kalman filter formulas in the
n-dimensional case.

![](img/paste-b2509bb74158604b75ba4fea38c6f36a76c16d49.jpg)  
  
Recap:  
![](img/paste-2bedb2cf129efdbf66ef9ea627b2f4d22fe1cf1c.jpg)  
![](img/paste-5d791c01a28629ca2a89e53e93b70bc7f8ffb149.jpg)


### Give the formulas for the measurement update in the  **extended** _  Kalman
filter._

![](img/paste-a82ece0a22b961a71346e8798efabd7e792a9c77.jpg)  
  
Recap:  
![](img/paste-76b70d5d71721c3fb4c623b2f6b20b1b58c666f4.jpg)  
![](img/paste-ef516937a0de47a316193e6671d3e4c5e5ac35e1.jpg)  
![](img/paste-9521b8f2e338ddf647045c571e736105bec5f58c.jpg)  
![](img/paste-8edec563e541e504c81f8172f4f2a83e91774e3e.jpg)


### Give an overview over the formulas of the extended Kalman filter.

![](img/paste-0678305e14a755c3b82f6281641de739c89a8f07.jpg)  
![](img/paste-a82ece0a22b961a71346e8798efabd7e792a9c77.jpg)  
  
Recap: General Kalman Filter Formulas  
![](img/paste-b87c92a9ce7d9131bb0d4effe30dceca83d03caa.jpg)


### What is the idea of the EKF? Describe it using the time update formulas.

![](img/paste-081125f9fd6e2ccc50a2d556c2892e33788d2ba5.jpg)


### What are disadvantages of the EKF?

  * Might diverge for severse nonlinearities
  * need to compute jacobians


### Given an overview over the classification of  **uncertain   _imperfect_
information**.

![](img/paste-42b2b6947cc76cd963b234ccead21ca722ed3eec.jpg)


